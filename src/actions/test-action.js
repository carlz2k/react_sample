import { ADD_ARTICLE } from './const';

export const addArticle = article => ({ type: ADD_ARTICLE, payload: article });

