import { ADD_ARTICLE } from '../actions/const';

const initialState = {
    articles: []
};
const testReducer = (state = initialState, action) => {
    switch (action.type) {
    case ADD_ARTICLE:
      return { ...state, articles: state.articles.concat(action.payload) };
    default:
      return state;
  }
};

export default testReducer;
