import { createStore } from 'redux';
import testReducer from '../reducers/test-reducer';

const testStore = createStore(testReducer);

export default testStore;
