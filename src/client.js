import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import App from './containers/App';
import testStore from './stores/test-store';
import { addArticle } from './actions/test-action';
import 'bootstrap/dist/css/bootstrap.css';

window.testStore = testStore;
window.addArticle = addArticle;

ReactDOM.render(
  <AppContainer>
    <Provider store={testStore}>
      <App />
    </Provider>
  </AppContainer>,
  document.getElementById('app')
);

if (module.hot) {
  module.hot.accept('./containers/App', () => {
    const NextApp = require('./containers/App').default; // eslint-disable-line global-require

    ReactDOM.render(
      <AppContainer>
        <Provider store={testStore}>
          <NextApp />
        </Provider>
      </AppContainer>,
      document.getElementById('app')
    );
  });
}
